for file in *.cpp; do 
        no_ext=${file%.*}
        g++ -c $file -o $no_ext.o
done
# Merge all the mutdb files in mutants folder
cat mutants/*mutdb > mutants/.symbols.mutdb
for file in unit_tests/*; do
    no_ext=${file%.*}
    # Compile the default unit test
    g++ -I ".admin_files" -I "." $file *.o -o .admin_files/build/$(basename $no_ext)

done


