#include "test_utils.hpp"
#include "maze.h"

int main(const int argc, const char **argv)
{
  return test_wrapper(argc, argv, []() {
    bool result = false;
    /**
     * Do stuff with student code here, assigning the test result to
     * the "result" variable...
     */
     int sol[4][4];
     return isSafe(sol, 1, 1);
     
  });
}

