#include "test_utils.hpp"
#include "maze.h"

int main(const int argc, const char **argv)
{
  return test_wrapper(argc, argv, []() {
    bool result = false;
    /**
     * Do stuff with student code here, assigning the test result to
     * the "result" variable...
     */
     int maze[4][4];
     solveMaze(maze);
     return 1;
  });
}

