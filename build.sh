for file in *.cpp; do 
        no_ext=${file%.*}
        g++ -c $file -o $no_ext.o
done

for file in unit_tests/*; do
    no_ext=${file%.*}
    g++ -I ".admin_files" -I "." $file *.o -o .admin_files/build/$(basename $no_ext)
    
    for file in mutants/*; do
       g++ -I ".admin_files" -I "." $file *.o -o .admin_files/build/$(basename $no_ext)
    done

done


