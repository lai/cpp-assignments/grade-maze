#ifndef MAZE_H
#define MAZE_H
/* C/C++ program to solve Rat in a Maze problem using
   backtracking */
#include <stdio.h>
#include <stdbool.h>
 
// Maze size
#define N 4

#ifdef __cplusplus
extern "C" {
#endif

bool solveMazeUtil(int maze[N][N], int x, int y, int sol[N][N]);

/* A utility function to print solution matrix sol[N][N] */
void printSolution(int sol[N][N]);
 
/* A utility function to check if x, y is valid index for N*N maze */
bool isSafe(int maze[N][N], int x, int y);
 
/* This function solves the Maze problem using Backtracking.  It mainly
   uses solveMazeUtil() to solve the problem. It returns false if no
   path is possible, otherwise return true and prints the path in the
   form of 1s. Please note that there may be more than one solutions,
   this function prints one of the feasible solutions.*/
bool solveMaze(int maze[N][N]);
 
#ifdef __cplusplus
}
#endif
#endif