for file in *.cpp; do 
        no_ext=${file%.*}
        g++ -c $file -o $no_ext.o
done
# Merge all the mutdb files in mutants folder
cat mutants/*mutdb > mutants/.symbols.mutdb
for file in unit_tests/*; do
    no_ext=${file%.*}
    # Compile the mutant unit test
    func=$(basename $no_ext)
    # strip test_ from the name of the file
    func=${func#test_}

    # Find the files with the symbol

    i=0
    while read mutated_shared_object; do
        mutated_no_ext=${mutated_shared_object%.*}
        mutated_object=.admin_files/build/${mutated_no_ext}.o
        mutated_source=mutants/${mutated_no_ext}.cpp
        
        g++ -I "." -c $mutated_source -o $mutated_object
        # Create an array of object files that excludes the mutated symbol
        objects=($mutated_object)
        for object in *.o; do
            if objdump -D $object | grep "<$func>" >/dev/null; then
                :
            else
                objects+=($object)
            fi
        done  
         
        g++ -I "." -I ".admin_files" $file ${objects[@]} -o .admin_files/build/mutants/${i}_test_${func}  
        echo "Compiling mutant ${i}_$func"
        
        ((i=i+1))
        
    done < <(cat mutants/.symbols.mutdb | grep "$func" | awk '{ print $7 }')
    

done


